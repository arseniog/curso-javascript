document.addEventListener('DOMContentLoaded', () => {

  const callback = characters => {

    let cadena = "";

    for (let i= 0; i< characters.length; i++) {

      cadena += "<option value ='" + characters[i].id + "'>" + characters[i].name + "</option>";

    }

    document.querySelector("#personaje1").innerHTML = cadena;
    document.querySelector("#personaje2").innerHTML = cadena;
  };

  api.characters(callback);
});

document.getElementById("boton-buscar").addEventListener('click', (event) => {

  const personaje1 = document.getElementById("personaje1").value;
  const personaje2 = document.getElementById("personaje2").value;

  event.preventDefault();
  document.querySelector("#resultados tbody").innerHTML = "";

    const callback = response => {

      const callbackII = responseII => {

        for (let i= 0; i< response.length; i++) {
          for (let j=0; j< responseII.length; j++) {
            if (response[i].id === responseII[j].id) {
              let celda1 = document.createElement("td");
              let celda2 = document.createElement("td");
              let celda3 = document.createElement("td");
              let textoCelda1 = document.createTextNode(responseII[j].id);
              let textoCelda2 = document.createTextNode(responseII[j].title);
              let textoCelda3 = document.createTextNode(responseII[j].characters);
              celda1.appendChild(textoCelda1);
              celda2.appendChild(textoCelda2);
              celda3.appendChild(textoCelda3);
              let fila = document.createElement("tr");
              fila.appendChild(celda1);
              fila.appendChild(celda2);
              fila.appendChild(celda3);
              document.querySelector('#resultados tbody').appendChild(fila);
            }
          }
        }
      };

    api.comics(personaje2, callbackII);
  };

  api.comics(personaje1, callback);
});