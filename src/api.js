const api = {
  characters: callback => $.get('/data/characters.json', characters => callback(characters)),
  comics: (characterId, callback) => $.get('/data/comics-' + characterId + '.json', response => callback(response))
};

/*
git add -A
git commit -am"Ejercicio terminado"
git push
 */