# Documentacion

[Aquí](https://drive.google.com/open?id=0BwkYx3waNF8VTGx3TzVwU3pkSUE)

# Requisitos

Para poder trabajar necesitas tener instalado uno de estos gestores de dependencias de NodeJS:

 * [Yarn](https://yarnpkg.com)
 * [NPM](https://www.npmjs.com)

# Descarga de dependencias

 * Si tienes Yarn instalado en tu equipo, puedes ejecutar el comando `yarn` en el directorio del proyecto.
 * Si no, ejecuta el comando `npm install` en el directorio del proyecto.

# Branches

## Paso 1 (paso1)

En este paso está la solución sencilla al problema. Todo en un fichero y usando jquery y javascript de manera muy básica

## Paso 2 (paso2)

En este paso hemos extraído cada cosa a un fichero distinto y hemos aplicado los distintos patrones de creación de objetos

## Paso 3 (paso3)

Ahora hemos aplicado los conceptos de namespacing y modularidad

## Paso 4 (paso4)

En este paso hemos mejorado el código asíncrono

## Paso 5 (paso5)

Ahora se trabajan las colecciones de datos de una manera más funcional

## Paso 6 (paso6)

En este branch hay un ejemplo de un test unitario

## Variante SystemJS (systemjs)

En este branch usamos SystemJS para resolver la modularidad completamente.

## Variante Ecmascript2015 / ES6 (es6)

En este branch hemos migrado todo el código del branch `systemjs` usando nuevas funcionalidades de ES6
